<?php
$bavotasan_theme_options = bavotasan_theme_options();
$format = get_post_format();
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		    <?php get_template_part( 'content', 'header' ); ?>

			<?php if ( has_post_thumbnail() && ! is_single() && 'excerpt' == $bavotasan_theme_options['excerpt_content'] ) { ?>
				<a href="<?php the_permalink(); ?>" class="image-anchor">
					<?php the_post_thumbnail( 'thumbnail', array( 'class' => 'alignleft img-thumbnail' ) ); ?>
				</a>
			<?php } ?>

		    <div class="entry-content">
			    <?php
				if ( 'excerpt' == $bavotasan_theme_options['excerpt_content'] && empty( $format ) && ( ! is_single() || is_search() || is_archive() ) ) {
					the_excerpt();
				} else {
					the_content( __( 'Read more &rarr;', 'snowblind' ) );
				}
				?>
		    </div><!-- .entry-content -->

		    <?php get_template_part( 'content', 'footer' ); ?>

	</article><!-- #post-<?php the_ID(); ?> -->