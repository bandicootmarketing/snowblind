<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 * and the left sidebar conditional
 *
 * @since 1.0.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<div id="page" class="container">
		<div id="mobile-menu" class="clearfix">
			<a href="#" class="left-menu pull-left"><i class="icon-reorder"></i></a>
			<a href="#" class="pull-right"><i class="icon-search"></i></a>
		</div>
		<div id="drop-down-search"><?php get_search_form(); ?></div>

		<header id="header">
			<div class="title-wrap">
				<?php $tag = ( is_front_page() && is_home() ) ? 'h1' : 'div'; ?>
				<<?php echo $tag; ?> id="site-title"><a href="<?php echo esc_url( home_url() ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></<?php echo $tag; ?>>
				<div id="site-description"><?php bloginfo( 'description' ); ?></div>
			</div>

			<?php
			if ( ! dynamic_sidebar( 'header-area' ) ) :
				?>
				<aside class="header-widget widget_search">
					<?php get_search_form(); ?>
				</aside>
				<?php
			endif;
			?>

			<div id="nav-wrapper">
				<div class="nav-content">
					<nav id="site-navigation" role="navigation">
							<h3 class="screen-reader-text"><?php _e( 'Main menu', 'snowblind' ); ?></h3>
						<a class="screen-reader-text" href="#primary" title="<?php esc_attr_e( 'Skip to content', 'snowblind' ); ?>"><?php _e( 'Skip to content', 'snowblind' ); ?></a>
						<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => '', 'depth' => 2 ) ); ?>
					</nav>
				</div>
			</div>
			<a href="<?php bloginfo( 'rss2_url' ); ?>" class="rss-link"><img src="<?php echo BAVOTASAN_THEME_URL; ?>/library/images/rsstop.png" alt="Subscribe to RSS Feed" /></a>
		</header>

		<div class="tri-top"></div>

		<div id="main">
			<div class="row">
