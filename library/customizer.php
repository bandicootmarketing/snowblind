<?php
/**
 * Set up the default theme options
 *
 * @since 1.0.0
 */
function bavotasan_default_theme_options() {
	//delete_option( 'theme_mods_snowblind' );
	return array(
		'width' => '960',
		'layout' => '2',
		'primary' => 'col-sm-8',
		'display_author' => 'on',
		'display_date' => 'on',
		'display_comment_count' => 'on',
		'display_categories' => 'on',
		'excerpt_content' => 'content',
	);
}

function bavotasan_theme_options() {
	$bavotasan_default_theme_options = bavotasan_default_theme_options();

	$return = array();
	foreach( $bavotasan_default_theme_options as $option => $value ) {
		$return[$option] = get_theme_mod( $option, $value );
	}

	return $return;
}

class Bavotasan_Customizer {
	public function __construct() {
		add_action( 'customize_register', array( $this, 'customize_register' ) );

		//print_r(get_option( 'theme_mods_snowblind' ));
		if ( $theme_options = get_option( 'snowblind_theme_options' ) ) {
			$mods = get_option( 'theme_mods_snowblind' );
			update_option( 'theme_mods_snowblind', array_merge( $mods, $theme_options ) );
			delete_option( 'snowblind_theme_options' );
		}
	}

	/**
	 * Adds theme options to the Customizer screen
	 *
	 * This function is attached to the 'customize_register' action hook.
	 *
	 * @param	class $wp_customize
	 *
	 * @since 1.0.0
	 */
	public function customize_register( $wp_customize ) {
		$bavotasan_default_theme_options = bavotasan_default_theme_options();

		// Layout section panel
		$wp_customize->add_section( 'bavotasan_layout', array(
			'title' => __( 'Layout', 'snowblind' ),
			'priority' => 35,
		) );

		$wp_customize->add_setting( 'width', array(
			'default' => $bavotasan_default_theme_options['width'],
            'sanitize_callback' => 'esc_attr',
		) );

		$wp_customize->add_control( 'width', array(
			'label' => __( 'Site Width', 'snowblind' ),
			'section' => 'bavotasan_layout',
			'priority' => 10,
			'type' => 'select',
			'choices' => array(
				'1200' => __( '1200px', 'snowblind' ),
				'960' => __( '960px', 'snowblind' ),
			),
		) );

		$wp_customize->add_setting( 'layout', array(
			'default' => $bavotasan_default_theme_options['layout'],
            'sanitize_callback' => 'esc_attr',
		) );

		$wp_customize->add_control( 'layout', array(
			'label' => __( 'Site Layout', 'snowblind' ),
			'section' => 'bavotasan_layout',
			'priority' => 15,
			'type' => 'radio',
			'choices' => array(
				'1' => __( '1 Sidebar - Left', 'snowblind' ),
				'2' => __( '1 Sidebar - Right', 'snowblind' ),
				'6' => __( 'No Sidebars', 'snowblind' )
			),
		) );

		$choices =  array(
			'col-sm-2' => '17%',
			'col-sm-3' => '25%',
			'col-sm-4' => '34%',
			'col-sm-5' => '42%',
			'col-sm-6' => '50%',
			'col-sm-7' => '58%',
			'col-sm-8' => '66%',
			'col-sm-9' => '75%',
			'col-sm-10' => '83%',
			'col-sm-12' => '100%',
		);

		$wp_customize->add_setting( 'primary', array(
			'default' => $bavotasan_default_theme_options['primary'],
            'sanitize_callback' => 'esc_attr',
		) );

		$wp_customize->add_control( 'primary', array(
			'label' => __( 'Main Content', 'snowblind' ),
			'section' => 'bavotasan_layout',
			'priority' => 20,
			'type' => 'select',
			'choices' => $choices,
		) );

		$wp_customize->add_setting( 'excerpt_content', array(
			'default' => $bavotasan_default_theme_options['excerpt_content'],
            'sanitize_callback' => 'esc_attr',
		) );

		$wp_customize->add_control( 'excerpt_content', array(
			'label' => __( 'Post Content Display', 'snowblind' ),
			'section' => 'bavotasan_layout',
			'priority' => 30,
			'type' => 'radio',
			'choices' => array(
				'excerpt' => __( 'Teaser Excerpt', 'snowblind' ),
				'content' => __( 'Full Content', 'snowblind' ),
			),
		) );

		// Posts panel
		$wp_customize->add_section( 'bavotasan_posts', array(
			'title' => __( 'Posts', 'snowblind' ),
			'priority' => 45,
		) );

		$wp_customize->add_setting( 'display_categories', array(
			'default' => $bavotasan_default_theme_options['display_categories'],
            'sanitize_callback' => array( $this, 'sanitize_checkbox' ),
		) );

		$wp_customize->add_control( 'display_categories', array(
			'label' => __( 'Display Categories', 'snowblind' ),
			'section' => 'bavotasan_posts',
			'type' => 'checkbox',
		) );

		$wp_customize->add_setting( 'display_author', array(
			'default' => $bavotasan_default_theme_options['display_author'],
            'sanitize_callback' => array( $this, 'sanitize_checkbox' ),
		) );

		$wp_customize->add_control( 'display_author', array(
			'label' => __( 'Display Author', 'snowblind' ),
			'section' => 'bavotasan_posts',
			'type' => 'checkbox',
		) );

		$wp_customize->add_setting( 'display_date', array(
			'default' => $bavotasan_default_theme_options['display_date'],
            'sanitize_callback' => array( $this, 'sanitize_checkbox' ),
		) );

		$wp_customize->add_control( 'display_date', array(
			'label' => __( 'Display Date', 'snowblind' ),
			'section' => 'bavotasan_posts',
			'type' => 'checkbox',
		) );

		$wp_customize->add_setting( 'display_comment_count', array(
			'default' => $bavotasan_default_theme_options['display_comment_count'],
            'sanitize_callback' => array( $this, 'sanitize_checkbox' ),
		) );

		$wp_customize->add_control( 'display_comment_count', array(
			'label' => __( 'Display Comment Count', 'snowblind' ),
			'section' => 'bavotasan_posts',
			'type' => 'checkbox',
		) );
	}

	/**
	 * Sanitize checkbox options
	 *
	 * @since 1.0.2
	 */
    public function sanitize_checkbox( $value ) {
        if ( 'on' != $value )
            $value = false;

        return $value;
    }
}
$bavotasan_customizer = new Bavotasan_Customizer;