= Snowblind =

* by c.bavota http://bavotasan.com/
* based on the Gridiculous Responsive Grid Boilerplate http://gridiculo.us/

== ABOUT SNOWBLIND ==

Create a truly unique design with Snowblind, a lightweight and fully responsive HTML5 theme. Use the new Theme Options customizer to add your own background, page layout, site width and more. Distinguish each post with one of the eight supported post formats: Video, Image, Aside, Status, Audio, Quote, Link and Gallery. Install JetPack to display each of your galleries through a tiled view and jQuery carousel. Compatible with bbPress & BuddyPress. Built using SASS, Compass and Twitter Bootstrap. Snowblind uses Google Fonts for improved typeface readability and works perfectly in desktop browsers, tablets and handheld devices. For a live demo go to http://demos.bavotasan.com/snowblind/.

== NOTES ==

* If you have a drop down list for your site navigation, the parent item should only be a grouping title with a "#" value for the URL parameter.
* The navbar only allows for a depth of one drop down level.

== LICENSE ==

Bootstrap - http://getbootstrap.com/
License: [[http://www.apache.org/licenses/LICENSE-2.0|Apache License v2.0]]

SASS - http://sass-lang.com/
License: [[http://opensource.org/licenses/MIT|MIT]]
Copyright: Hampton Catlin, Nathan Weizenbaum

html5shiv - http://code.google.com/p/html5shiv/
License: [[http://opensource.org/licenses/MIT|MIT]]

Font Awesome - http://fortawesome.github.io/Font-Awesome/
License: [[http://opensource.org/licenses/MIT|MIT]]
Copyright: Dave Gandy, https://twitter.com/davegandy

Google Fonts - http://www.google.com/fonts/
License: [[http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL|SIL Open Font License]]

Stock Images - http://unsplash.com/
Licensed under the terms of CC0

== CHANGELOG ==

03/02/2016 v2.0.3
* Added about page
* Updated preview page
* Fixed H1 tag issue
* Fixed Customizer save issue
* Updated footer credit
* Added prev/next link to pages
* Added HTML5 shiv
* Added title-tag support
* Removed harvey.js
* Updated screenshot
* Updated mobile menu
* Updated language file

11/12/2013 v2.0.2.1
* Removed message bar as per WP.org request

10/22/2013 v2.0.2
* Fixed responsive videos
* Added message bar
* Removed customize menu item
* Removed unnecessary files and functions
* Added tag description to archive page
* Tweaked 404 CSS
* Updated font icons
* Added drop down menu
* Tweaked preview pro text
* Added expanding header search
* Updated language files

09/11/2013 v2.0.1
* Fixed responsive columns
* Updated CSS to fix alignment
* Fixed mobile menu JS
* Added CSS to fix bbPress issues
* Updated 404 CSS
* Updated offcanvas CSS

08/29/2013 v2.0.0
* Rebuilt codebase
* Incorporated Twitter Bootstrap 3.0
* Added responsive layout
* Updated for WordPress 3.6
* Created customizer options
* Increased site width options to 960px and 1200px
* Added support for bbPress and BuddyPress
* Improved design elements
* Added Google Fonts
* Updated language file
* Added readme.txt

11/2/2010 v1.1.5
* Fixed textdomain issue for languages
* Updated language files

11/1/2010 v1.1.4
* Actually fixed weird issue with certain plugins not liking that I used get_the_excerpt() for the meta description
* Added display tag line option for header
* Update some code
* Updated admin JS

10/26/2010 v1.1.3
* Updated for WordPress 3.0
* Upgrade Arturo Theme Engine v2.0
* Updated language .po file

11/09/2009 v1.1.2
* Fixed Header Logo bug

10/31/2009 v1.1.1
* Fixed Extended Footer bug

10/27/2009 v.1.1
* Fixed IE Bugs
* Translation Ready (.po & .mo files)
* Changed the header uploader to a direct file path
* Page nav menu inclusion option

07/27/2009 Public release